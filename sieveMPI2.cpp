#include <vector>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#define MAESTRO 0
#define MAX_SIZE 1000010LL
using namespace std;

typedef long long ll;
typedef vector<ll> vll;
typedef vector<int> vi;


int main(int argc, char const *argv[])
{
	int comm_sz;
	int my_rank;
	ll contador = 0;

	ll tamanio_criba = 1000000;
	vll listaDePrimos;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	ll numerosRestantes = (tamanio_criba) - (sqrt(tamanio_criba)) + 1;
	ll elementosPorProceso = numerosRestantes / comm_sz;
	ll indiceInferior = (my_rank * elementosPorProceso) + sqrt(tamanio_criba) + 1;

	
	my_rank == comm_sz - 1 ? elementosPorProceso += (numerosRestantes % comm_sz) : false;

	ll indiceSuperior = indiceInferior + elementosPorProceso - 1;

	
	vll esPrimoP0 (sqrt(tamanio_criba + 1), 1);
	ll esPrimoPk[MAX_SIZE];
	
	for (ll i = 0; i < MAX_SIZE; ++i) esPrimoPk[i] = 1;

	esPrimoPk[0] = esPrimoPk[1] = 0;
	esPrimoP0[0] = esPrimoP0[1] = 0;

	for (ll i = 2; i <= sqrt(tamanio_criba + 1); ++i)
	{
		if (esPrimoP0[i])
		{
			for (ll j = i * i; j <= sqrt(tamanio_criba); j += i)
				esPrimoP0[j] = 0;
			listaDePrimos.push_back(i);

			for(ll j = indiceInferior; j <= indiceSuperior; j++)
				if(j % i == 0)
					esPrimoPk[j - indiceInferior ] = 0;
		}
	}

	if (my_rank == MAESTRO) {
		ll ci;
		ll cs;
		for (ll i = 0; i < (int)listaDePrimos.size(); ++i) {
			//cout << listaDePrimos[i] << endl;
			contador++;
		}

		for(ll i = indiceInferior; i <= indiceSuperior; i++) 
			if(esPrimoPk[i - indiceInferior]) {
				//cout << i << endl; 
				contador++;
			} 

		for(int proceso = 1; proceso < comm_sz ; proceso++) {

			MPI_Recv(&ci, 1, MPI_LONG_LONG, proceso, MAESTRO, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			MPI_Recv(&cs, 1, MPI_LONG_LONG, proceso, MAESTRO, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			proceso == comm_sz - 1 ? elementosPorProceso += (numerosRestantes % comm_sz) : false;

			MPI_Recv(&esPrimoPk, elementosPorProceso, MPI_LONG_LONG, proceso, MAESTRO, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			for(ll iterador = ci; iterador <= cs; iterador++) {
				if(esPrimoPk[iterador - ci]) {
					contador++;
					//cout << iterador << endl;
				}
			}
		}
		printf("Primos menores a %lld = %lld\n", tamanio_criba, contador);
	}

	else {
		MPI_Send(&indiceInferior, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&indiceSuperior, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
		MPI_Send(&esPrimoPk, elementosPorProceso, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
	}
	
	MPI_Finalize();
	return 0;
}