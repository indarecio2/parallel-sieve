#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <bitset>
#include <omp.h>
using namespace std;

typedef long long ll;
typedef vector<long long> vll;

bitset<100000010> esPrimo;
vll listaPrimos;


void Hello();

int main(int argc, char const *argv[]) {
	int thread_count = strtol(argv[1], NULL, 10);

	ll tamanio_criba = 1000000;
	esPrimo.set();
	esPrimo[0] = esPrimo[1] = 1;
	listaPrimos.clear();

	for (ll i = 2; i <= tamanio_criba + 1; i++) {
		if (esPrimo[i]) {
			# pragma omp parallel for num_threads(thread_count) 
			for (ll j = i * i; j <= tamanio_criba + 1; j += i) {
				esPrimo[j] = 0;
			}
			listaPrimos.push_back(i);
		}
	}

	printf("Primos menores o iguales a %lld %ld\n", tamanio_criba, listaPrimos.size());
	return 0;
}

