#include <iostream>
#include <bitset>
#include <pthread.h>
#include <vector>
#include <cmath>
#include <stack>
#include <queue>
#include <algorithm>
using namespace std;


typedef long long ll;
typedef vector<ll> vll;

ll tamanio_criba;
bitset<100000010> esPrimo;
priority_queue<ll> primosOrdenados;
vll listaPrimos;


int thread_count;
pthread_mutex_t count_mutex;

struct argumentosThread
{
	int idThread;
	ll primoPivote;
};


void *cribaProcesoK(void *argThread) {

	argumentosThread *dt;

	dt = (argumentosThread *) argThread;
	//int id = dt -> idThread;
	ll primo =  dt -> primoPivote; 

	for (ll j = primo *  primo; j <= tamanio_criba + 1; j += primo) {
		esPrimo[j] = 0;
	}
	if (primo <= tamanio_criba) {
		pthread_mutex_lock(&count_mutex);
		primosOrdenados.push(primo);
		pthread_mutex_unlock(&count_mutex);
	}
	return NULL;
}

bool isPrime(const ll &n) {
	ll r = sqrt(n);
	if (n == 2) return true;
	if (n % 2 == 0) return false;
	for (ll i = 3; i <= r; i += 2) 
		if(n % i == 0)
			return false;
	return true;
}


void calculaKNumerosPrimos (vector<ll> &kPrimos, const int &numeroDeProcesos, const ll &ultimoPrimo) {
	ll i = ultimoPrimo + 1;
	int contador = 0;
	kPrimos.clear();
	while (!esPrimo[i] && i < (int)esPrimo.size()){i++;};
	
	for (; contador < (int)numeroDeProcesos; ++i) {
		if (isPrime(i)) {
			kPrimos.push_back(i);
			contador++;
		}
	}
}


void imprimeLista (priority_queue<ll> &lprimos) {
	stack<ll> listaPrimosImprime;
	while (!lprimos.empty()) {
		listaPrimosImprime.push(lprimos.top());
		lprimos.pop();
	}
	while (!listaPrimosImprime.empty()) {
		printf("%lld\n", listaPrimosImprime.top());
		listaPrimosImprime.pop();
	}
}

int main(int argc, char const *argv[])
{
	argumentosThread* argThread;
	pthread_t* thread_handles;

	thread_count = strtol(argv[1], NULL, 10);
	thread_handles = new pthread_t[thread_count];
	argThread = new argumentosThread[thread_count];
	
	esPrimo.set();
	esPrimo[0] = esPrimo[1] = 0;
	tamanio_criba = 1000000;
	vector<ll> kprimos;
	
	calculaKNumerosPrimos(kprimos, thread_count, 0);
	while(kprimos[0] < (int)tamanio_criba and !kprimos.empty()) {
		for (int thread = 0; thread < (int)kprimos.size(); thread++) {
			argThread[thread].idThread = thread;
			argThread[thread].primoPivote = kprimos[thread];
			pthread_create(&thread_handles[thread], NULL, cribaProcesoK, (void *) &argThread[thread]);
		}

		for (long thread = 0; thread < (int)thread_count; thread++)
		{
			pthread_join(thread_handles[thread], NULL);
		}
		calculaKNumerosPrimos(kprimos, thread_count, primosOrdenados.top());
	}
	printf("Primos menores a %lld = %ld\n", tamanio_criba, primosOrdenados.size());
	//imprimeLista(primosOrdenados);	
	delete[] argThread;	
	delete[] thread_handles;
	return 0;
}